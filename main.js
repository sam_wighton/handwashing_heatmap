
var hover_data = {};
var true_width = 0;
var true_image_count = 30;

var image_offsets = [];

var sample_currently_displayed = -1;


var w;
var h;

var w_shrink;
var h_shrink;

function main() {
    const shrinkage = 3;
    const image = document.getElementById('image_1');
    // console.log("inside main");
    image.addEventListener('load', e => {
        // console.log("loading image");
    });
    w = image.width;
    h = image.height;

    w_shrink = Math.round(w / shrinkage);
    h_shrink = Math.round(h / shrinkage);
    // console.log("median = ", get_median([1,2,3,4,5]));

    const canvas = document.getElementById('canvas');
	canvas.addEventListener('click', mouse_click);

    canvas.width = w_shrink;
    canvas.height = h_shrink;

    display_next_image();
}

function display_next_image() {
    sample_currently_displayed += 1;

    const canvas = document.getElementById('canvas');

    if (sample_currently_displayed >= true_image_count) {
        canvas.removeEventListener('click', mouse_click);
        process_heat_map();

        // console.log(image_offsets);
        return;
    }

    const sample_image = document.getElementById('image_' + (sample_currently_displayed + 1));
    const ctx = canvas.getContext('2d');
    ctx.drawImage(sample_image, 0, 0, w, h, 0, 0, w_shrink, h_shrink);
    image_data = ctx.getImageData(0, 0, w_shrink, h_shrink);
    ctx.putImageData(image_data, 0, 0);

}

function process_heat_map() {

    document.getElementById('tip').textContent = "";

    const canvas = document.getElementById('canvas');
    const image_count = true_image_count;
    const bump_ammount = 255 / image_count;

    var image_data;
    const ctx = canvas.getContext('2d');

	canvas.addEventListener('mousemove', mouse_move);

    image_data = ctx.getImageData(0, 0, w_shrink, h_shrink);

    var pixel_arrays = [];
    var temp_offset = 10;

    for (var j = 0; j < image_count; j++) {
        const image = document.getElementById('image_' + (j + 1));
        //TODO draw at 0,0 every time
        ctx.drawImage(image, 0, 0, w, h, 0, 0, w_shrink, h_shrink);
        image_data = ctx.getImageData(0, 0, w_shrink, h_shrink);
        var pixels_to_slice = (image_offsets[j].x - 250) + (image_offsets[j].y - 30) * w_shrink;
        var shifted_pixels = image_data.data.slice(0,pixels_to_slice * 4);
        var remaining_pixels = image_data.data.slice(pixels_to_slice * 4 + 1);
        pixel_arrays.push(new Uint8ClampedArray([...remaining_pixels,...shifted_pixels]));

    }

    // console.log(pixel_arrays);

    // // ------------------------------------------------
    // ctx.drawImage(image, 0, 0);
    // image_data = ctx.getImageData(0, 0, w, h);
    // // const data_1 = [...image_data.data];
    // pixel_arrays.push([...image_data.data]);
    // // const data_1 = image_data.data.map(x => x / image_count);

    // // ------------------------------------------------
    // ctx.drawImage(image, 20, 20);
    // image_data = ctx.getImageData(0, 0, w, h);
    // const data_2 = [...image_data.data];

    // // ------------------------------------------------

    const blank_canvas = image_data.data;

    const median_count = 8;
    // const median_count = 1;

    // console.log( blank_canvas.length);
    for (var i = 0; i < blank_canvas.length; i += 4) {

        hover_data[i/4] = 0;

        if (i + median_count * 4 >= blank_canvas.length) {
            break;
        }

        var f = 0;
        for (var j = 0; j < image_count; j++) {
            var median_array = [];
            for (var k = 0; k < median_count; k += 5) {
                median_array.push(pixel_arrays[j][i + k * 4]);
            }
            // f -= get_average(median_array) / image_count;
            if (get_median(median_array) < 128) { //red pixel
                f += bump_ammount;
                hover_data[i/4] += 1;
            }
        }
        if (i / 4 / w_shrink > h_shrink - 18) {
            var x_offset = i / 4 % w_shrink;
            if (x_offset == Math.round(0 * w_shrink / 5)) {f = 255}
            if (x_offset == Math.round(1 * w_shrink / 5)) {f = 255}
            if (x_offset == Math.round(2 * w_shrink / 5)) {f = 255}
            if (x_offset == Math.round(3 * w_shrink / 5)) {f = 255}
            if (x_offset == Math.round(4 * w_shrink / 5)) {f = 255}
            if (x_offset == w_shrink - 1) {f = 255}
        }
        if (i / 4 / w_shrink > h_shrink - 15) {
            f = (i / 4 %  w_shrink) / w_shrink * 255;
        }
        blank_canvas[i + 0] = Math.round(f*3);
        blank_canvas[i + 1] = Math.round((f - 85)*3);
        blank_canvas[i + 2] = Math.round((f - 170)*3);
        // if ((i/4) % (w * 200) == 0) {
        //     ctx.putImageData(image_data, 0, 0);
        //     console.log("drawing to canvas");
        // }
    }
    ctx.putImageData(image_data, 0, 0);

    ctx.font = '20px sanserif';
    ctx.fillStyle = "#fff";
    ctx.textBaseline = "bottom";
    ctx.textAlign = "left";
    ctx.fillText('0%', 0 * w_shrink / 5, h_shrink - 20);
    ctx.textAlign = "center";
    ctx.fillText('20%', 1 * w_shrink / 5, h_shrink - 20);
    ctx.fillText('40%', 2 * w_shrink / 5, h_shrink - 20);
    ctx.fillText('60%', 3 * w_shrink / 5, h_shrink - 20);
    ctx.fillText('80%', 4 * w_shrink / 5, h_shrink - 20);
    ctx.textAlign = "right";
    ctx.fillText('100%', 5 * w_shrink / 5, h_shrink - 20);

    // console.log(data_1);
}

function mouse_click(event) {

	var canvas = document.getElementById('canvas');

    var rel_x = event.clientX - canvas.offsetLeft;
    var rel_y = event.clientY - canvas.offsetTop;

    //TODO add offset info then display next image
    image_offsets.push({x: rel_x, y: rel_y});
    display_next_image();
}


function mouse_move(event) {

	var canvas = document.getElementById('canvas');

    var rel_x = event.clientX - canvas.offsetLeft;
    var rel_y = event.clientY - canvas.offsetTop;

    // console.log(rel_x,rel_y);
    // console.log(hover_data[canvas.width * rel_y + rel_x]);
    document.getElementById('hover_info').textContent = hover_data[canvas.width * rel_y + rel_x] + ' / ' + true_image_count;
    draw_slider(hover_data[canvas.width * rel_y + rel_x], true_image_count);

}

function draw_slider(current,total) {

    const canvas = document.getElementById('canvas_slider');
    canvas.width = w_shrink;
    canvas.height = 8;
    const ctx = canvas.getContext('2d');

    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    ctx.fillStyle = "black";
    ctx.fillRect((w_shrink * current / total) - 1, 0, 2, canvas.height);
}

function get_median(x) {
    // console.log("length = ", x.length / 2);
    return x.sort(
        function(a,b){
            return a - b;
        })[Math.floor(x.length / 2)];
}

function get_max(x) {
    // console.log("length = ", x.length / 2);
    return x.sort(
        function(a,b){
            return a - b;
        })[x.length -1];
}
function get_average(x) {
    var total = 0;
    for (var i = 0; i < x.length; i++) {
        total += x[i];
    }
    return total / x.length;
}

window.addEventListener("load", main);
